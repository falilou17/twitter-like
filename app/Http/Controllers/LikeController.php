<?php

namespace App\Http\Controllers;
use App\Like;
use App\Message;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;
use Illuminate\Http\Request;

class LikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            Like::create([
                'message_id' => $request->input('message_id'),
                'user_id' => Auth::user()->getAuthIdentifier()
            ]);
            return redirect()
                ->back()
                ->with('type', 'success')
                ->with('msg', 'You liked the message');
        }catch (Exception $e){
            return redirect()
                ->back()
                ->with('type', 'error')
                ->with('msg', 'An error has occurred');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $message = Message::findOrFail($id);
            $like = $message->likes()->where('user_id', Auth::user()->getAuthIdentifier())->first();
            if (!empty($like)){
                $like->delete();
                return redirect()
                    ->back()
                    ->with('type', 'success')
                    ->with('msg', 'You do not like the message anymore');
            }else{
                return redirect()
                    ->back();
            }

        }catch (Exception $e){
            return redirect()
                ->back()
                ->with('type', 'error')
                ->with('msg', 'An error has occurred');
        }
    }
}
