<?php

namespace App\Http\Controllers;

use App\Friend;
use App\Http\Requests\UserResquest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;

class UserController extends Controller
{

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = Auth::user();
        return view('user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserResquest $request
     * @return array|string
     */
    public function update(UserResquest $request)
    {
        try {
            $user = Auth::user();
            $user['name'] = !empty($request->input('name')) ? $request->input('name') : $user['name'];
            if ($request->hasFile('picture')) {
                $picture = $request->file('picture');
                $pictureName = $user->getAuthIdentifier() . $picture->getExtension();
                $picture->move(public_path('pictures'), $pictureName);
                $user['picture'] = $pictureName;
            }
            $user->update();
            return redirect()
                ->route('user.profile', [$user->getAuthIdentifier()])
                ->with('type', 'success')
                ->with('msg', 'Profile updated !');
        } catch (Exception $e) {
            return redirect()
                ->back()
                ->withInput()
                ->with('type', 'error')
                ->with('msg', 'An error has occurred');
        }
    }


    /**
     * Display the user profile.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function profile($id)
    {
        try {
            $user = User::find($id);
            $followedUserId = $user->friends()->pluck('user2_id');
            $followersId = Friend::where('user2_id', $id)->pluck('user1_id');

            $followedUser = User::whereIn('id', $followedUserId)->get();
            $followers = User::whereIn('id', $followersId)->get();

            if (!empty($user)) {
                $itIsMyProfile = Auth::user()->getAuthIdentifier() == $id;
                $areFriends = !empty(Auth::user()->friends()->where('user2_id', $id)->first());
                return view('user.profile', compact('user', 'itIsMyProfile', 'areFriends', 'followedUser', 'followers'));
            } else
                return redirect()
                    ->back()
                    ->with('type', 'error')
                    ->with('msg', 'User not found !');
        } catch (Exception $e) {
            return redirect()
                ->back()
                ->with('type', 'error')
                ->with('msg', 'An error has occurred');
        }
    }

    /**
     * Follow a user.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function follow($id)
    {
        try {
            $friend = Friend::create([
                'user1_id' => Auth::user()->getAuthIdentifier(),
                'user2_id' => $id
            ]);
            $user = User::find($id);
            return redirect()
                ->back()
                ->with('type', 'success')
                ->with('msg', $user->name.' followed !');
        } catch (Exception $e) {
            return redirect()
                ->back()
                ->with('type', 'error')
                ->with('msg', 'An error has occurred');
        }
    }

    /**
     * Unfollow a user.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function unfollow($id)
    {
        try {
            $friend = Auth::user()->friends()->where('user2_id', $id)->first();
            $user = User::findOrFail($id);
            if(!empty($friend)){
                $friend->delete();
                return redirect()
                    ->back()
                    ->with('type', 'success')
                    ->with('msg', $user->name.' unfollowed !');
            }else
                return redirect()
                    ->back()
                    ->with('type', 'error')
                    ->with('msg', $user->name.' not followed !');
        } catch (Exception $e) {
            return redirect()
                ->back()
                ->with('type', 'error')
                ->with('msg', 'An error has occurred');
        }
    }
}
