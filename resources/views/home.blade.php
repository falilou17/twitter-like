@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img class="col-sm-6 col-sm-offset-3 img-circle img-responsive"
                                     src="{{ asset('pictures/'.$user->picture) }}" alt="profile picture">
                            </div>
                            <div class="col-sm-12 text-center">
                                <a href="{{ route('user.profile', [$user->id]) }}">{{ strtoupper($user->name) }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="alert alert-success">
                            {{ count($user->messages()->get()) > 1 ? count($user->messages()->get()).' publications' : count($user->messages()->get()).' publication' }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-sm-12">
                                <form method="post" action="{{ route('messages.store') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <textarea class="form-control" name="message" rows="2"
                                                  placeholder="What's up?"></textarea>
                                    </div>
                                    <button type="submit" class="col-md-offset-8 col-md-4 btn-xs btn-primary right">
                                        Submit
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                        @foreach($messages as $message)
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-sm-1 text-center">
                                            <a href="{{ route('user.profile', [$message->user->id]) }}">
                                                <img class="img-circle img-responsive"
                                                     src="{{ asset('pictures/'.$message->user->picture) }}"
                                                     alt="profile picture">
                                            </a>
                                        </div>
                                        <div class="col-sm-9">
                                            <b><a href="{{ route('user.profile', [$message->user->id]) }}">{{ $message->user->name }}</a></b>
                                            - {{ $message->created_at->diffForHumans() }}
                                        </div>
                                        <div class="col-sm-2 right">
                                            @if($message->user->id == $user->id)
                                                <form method="post"
                                                      action="{{ route('messages.destroy', [$message->id]) }}">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="delete">
                                                    <button type="submit" class="btn-xs btn-danger btn-block"><i
                                                                class="fa fa-trash"></i></button>
                                                </form>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <p>{{ $message->content }}</p>
                                    </div>
                                    <div class="col-md-offset-5 col-md-2">
                                        @if( count($message->likes()->where('user_id', $user->id)->get()) == 0)
                                            <form method="post" action="{{ route('likes.store') }}">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="message_id" value="{{ $message->id }}">
                                                <button class="btn-xs btn-block btn-success"><i
                                                            class="fa fa-heart-o"></i>
                                                    <b> {{ count($message->likes()->get()) }}</b></button>
                                            </form>
                                        @else
                                            <form method="post" action="{{ route('likes.destroy', [$message->id]) }}">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="delete">
                                                <button class="btn-xs btn-block btn-danger"><i class="fa fa-heart "></i>
                                                    <b> {{ count($message->likes()->get()) }}</b></button>
                                            </form>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
                    @if(session()->has('type'))
            var msg = '{{  session()->get('msg') }}';
            var type = '{{ session()->get('type') }}';
            swal(msg, "", type);
                    @endif
                    @if ($errors->has('message'))
            var msg = '{{  $errors->first('message') }}';
            swal(msg, "", 'error');
            @endif
        });
    </script>
@endsection