@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Edit Profile
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <ul class="list-group">
                                    <li class="list-group-item">Email <span class="pull-right">:</span></li>
                                    <li class="list-group-item">Name <span class="pull-right">:</span></li>
                                    <li class="list-group-item">Number post <span class="pull-right">:</span></li>
                                    <li class="list-group-item">Picture <span class="pull-right">:</span></li>
                                </ul>
                            </div>
                            <form enctype="multipart/form-data" method="post" action="{{ route('user.update') }}">
                                {{ csrf_field() }}
                                <div class="col-sm-8">
                                    <ul class="list-group text-center">
                                        <li class="list-group-item">{{ $user->email }}</li>
                                        <li class="list-group-item"><input class="form-control" name="name" type="text"
                                                                           value="{{ $user->name }}"></li>
                                        <li class="list-group-item">{{ count($user->messages()->get()) }}</li>
                                        <li class="list-group-item">
                                            <div class="row text-center">
                                                <div class="col-md-12">
                                                    <label for="hidden_input" class="control-label">
                                                        <img class="img-circle img-responsive" id="preview_picture"
                                                             src="{{ asset('pictures/'.$user->picture) }}"
                                                             alt="default">
                                                    </label>
                                                    <input onchange="readURL(this)" id="hidden_input" type="file"
                                                           class="form-control" name="picture">
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-success pull-right">Save changes</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#preview_picture').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
