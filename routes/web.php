<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function() {
    Route::resources([
        'messages' => 'MessageController',
        'likes' => 'LikeController',
    ]);


    Route::group(['prefix' => 'user'], function() {
        Route::get('/edit', ['as' => 'user.edit', 'uses' => 'UserController@edit']);
        Route::post('/update', ['as' => 'user.update', 'uses' => 'UserController@update']);
        Route::get('/{id}', ['as' => 'user.profile', 'uses' => 'UserController@profile']);
        Route::post('/follow/{id}', ['as' => 'user.follow', 'uses' => 'UserController@follow']);
        Route::post('/unfollow/{id}', ['as' => 'user.unfollow', 'uses' => 'UserController@unfollow']);
    });
});